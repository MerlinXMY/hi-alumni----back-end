package com.tensquare.search.service;

import com.tensquare.search.dao.SquareDao;
import com.tensquare.search.pojo.Square;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SquareService {

    @Autowired
    private SquareDao squareDao;

//    @Autowired
//    private IdWorker idWorker;

    public void save(Square article){

        squareDao.save(article);

    }

    public Page<Square> findByKey(String key, int page, int size) {
        Sort sort = new Sort(Sort.Direction.DESC,"publictime");
        Pageable pageable = PageRequest.of(page-1,size,sort);

        return squareDao.findByContenttextOrTagOrUsernicknameOrLocationstringLike(key,key,key,key,pageable);

    }
    public Page<Square> find(int page, int size) {
        Sort sort = new Sort(Sort.Direction.DESC,"publictime");
        Pageable pageable = PageRequest.of(page-1,size,sort);

        return squareDao.findAll(pageable);

    }
    public Page<Square> findToCheck(int page, int size,String state) {
        Sort sort = new Sort(Sort.Direction.DESC,"publictime");
        Pageable pageable = PageRequest.of(page-1,size,sort);

        return squareDao.findBySquarestate(state,pageable);

    }
}
