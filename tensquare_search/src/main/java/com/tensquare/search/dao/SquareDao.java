package com.tensquare.search.dao;

import com.tensquare.search.pojo.Square;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface SquareDao extends ElasticsearchRepository<Square,String> {

    public Page<Square> findByContenttextOrTagOrUsernicknameOrLocationstringLike(String contentText,String tag, String userNickname,String locationString, Pageable pageable);


    Page<Square> findBySquarestate(String squarestate, Pageable pageable);
}
