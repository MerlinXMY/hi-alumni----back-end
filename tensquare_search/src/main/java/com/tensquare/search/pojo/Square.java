package com.tensquare.search.pojo;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import java.io.Serializable;
import java.util.Date;

@Document(indexName = "tensquare_square", type = "square")
public class Square implements Serializable {

    //是否索引，就是看域能被搜索
    //是否分词，就表示搜索得时候是整体匹配还是单词匹配
    //是否存储，是否在页面上显示
    @Id
    private String id;

    private String square_class_id;
    private String user_id;
    private String user_sex;
    private String user_pic;

    @Field(index = true,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String usernickname;

    @Field(index = true,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String contenttext;

    private String content_image;
    private String content_video;
    private Date publictime;
    private String permission;
    private int visits_num;
    private int thumbup_num;
    private int comment_num;
    private int share_num;
    private String squarestate;
    private String videoimageurl;

    public String getVideoimageurl() {
        return videoimageurl;
    }

    public void setVideoimageurl(String videoimageurl) {
        this.videoimageurl = videoimageurl;
    }

    @Field(index = true,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String locationstring;

    private int user_age;
    private String tagId;

    @Field(index = true,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String tag;

    private String tagsState;
    private long count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getSquare_class_id() {
        return square_class_id;
    }

    public void setSquare_class_id(String square_class_id) {
        this.square_class_id = square_class_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_sex() {
        return user_sex;
    }

    public void setUser_sex(String user_sex) {
        this.user_sex = user_sex;
    }

    public String getUser_pic() {
        return user_pic;
    }

    public void setUser_pic(String user_pic) {
        this.user_pic = user_pic;
    }



    public String getContent_image() {
        return content_image;
    }

    public void setContent_image(String content_image) {
        this.content_image = content_image;
    }

    public String getContent_video() {
        return content_video;
    }

    public void setContent_video(String content_video) {
        this.content_video = content_video;
    }

    public Date getPublictime() {
        return publictime;
    }

    public void setPublictime(Date publictime) {
        this.publictime = publictime;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public int getVisits_num() {
        return visits_num;
    }

    public void setVisits_num(int visits_num) {
        this.visits_num = visits_num;
    }

    public int getThumbup_num() {
        return thumbup_num;
    }

    public void setThumbup_num(int thumbup_num) {
        this.thumbup_num = thumbup_num;
    }

    public int getComment_num() {
        return comment_num;
    }

    public void setComment_num(int comment_num) {
        this.comment_num = comment_num;
    }

    public int getShare_num() {
        return share_num;
    }

    public void setShare_num(int share_num) {
        this.share_num = share_num;
    }

    public String getSquarestate() {
        return squarestate;
    }

    public void setSquarestate(String squarestate) {
        this.squarestate = squarestate;
    }


    public int getUser_age() {
        return user_age;
    }

    public void setUser_age(int user_age) {
        this.user_age = user_age;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTagsState() {
        return tagsState;
    }

    public void setTagsState(String tagsState) {
        this.tagsState = tagsState;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getUsernickname() {
        return usernickname;
    }

    public void setUsernickname(String usernickname) {
        this.usernickname = usernickname;
    }

    public String getContenttext() {
        return contenttext;
    }

    public void setContenttext(String contenttext) {
        this.contenttext = contenttext;
    }

    public String getLocationstring() {
        return locationstring;
    }

    public void setLocationstring(String locationstring) {
        this.locationstring = locationstring;
    }
}
