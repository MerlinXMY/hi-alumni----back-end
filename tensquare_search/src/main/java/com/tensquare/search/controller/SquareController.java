package com.tensquare.search.controller;

import com.tensquare.search.pojo.Square;
import com.tensquare.search.service.SquareService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/square")
@CrossOrigin
public class SquareController {

    @Autowired
    private SquareService articleService;

    @RequestMapping(method = RequestMethod.POST)
    public Result save(@RequestBody Square article){

        articleService.save(article);

        return new Result(true, StatusCode.OK,"添加成功");


    }

    @RequestMapping(value = "/{key}/{page}/{size}",method = RequestMethod.GET)
    public Result findByKey(@PathVariable String key,@PathVariable int page,@PathVariable int size){

        Page<Square> pageData = articleService.findByKey(key,page,size);

        return new Result(true,StatusCode.OK,"查询成功",new PageResult<Square>(pageData.getTotalElements(),pageData.getContent()));



    }
    @RequestMapping(value = "/{page}/{size}",method = RequestMethod.GET)
    public Result find(@PathVariable int page,@PathVariable int size){

        Page<Square> pageData = articleService.find(page,size);

        return new Result(true,StatusCode.OK,"查询成功",new PageResult<Square>(pageData.getTotalElements(),pageData.getContent()));



    }
    @RequestMapping(value = "/findToCheck/{page}/{size}/{state}",method = RequestMethod.GET)
    public Result findToCheck(@PathVariable int page,@PathVariable int size,@PathVariable String state){
        System.out.println(state);
        Page<Square> pageData = articleService.findToCheck(page,size,state);

        return new Result(true,StatusCode.OK,"查询成功",new PageResult<Square>(pageData.getTotalElements(),pageData.getContent()));



    }

}
