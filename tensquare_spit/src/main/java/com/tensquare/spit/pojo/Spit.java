package com.tensquare.spit.pojo;


import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class Spit implements Serializable {
    @Id
    private String _id;
    private String content;
    private Date publishtime;
    private String userid;
    private String commmenteduserid;
    private String userPic;
    private String nickname;
    private String secondnickname;
    private Integer thumbup;
    private String state;
    private String parentid;
    private String squareId;

    public String getSecondnickname() {
        return secondnickname;
    }

    public void setSecondnickname(String secondnickname) {
        this.secondnickname = secondnickname;
    }

    public String getCommmenteduserid() {
        return commmenteduserid;
    }

    public void setCommmenteduserid(String commmenteduserid) {
        this.commmenteduserid = commmenteduserid;
    }

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }

    public String getSquareId() {
        return squareId;
    }

    public void setSquareId(String squareId) {
        this.squareId = squareId;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPublishtime() {
        return publishtime;
    }

    public void setPublishtime(Date publishtime) {
        this.publishtime = publishtime;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getThumbup() {
        return thumbup;
    }
    public void setThumbup(Integer thumbup) {
        this.thumbup = thumbup;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }
}
