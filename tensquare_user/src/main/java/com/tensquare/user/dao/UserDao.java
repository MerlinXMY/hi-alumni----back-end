package com.tensquare.user.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tensquare.user.pojo.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface UserDao extends JpaRepository<User,String>,JpaSpecificationExecutor<User>{
	public User findByMobile(String mobile);
    public User findByUsername(String username);
	@Modifying
	@Query(value = "update tb_user set fanscount = fanscount + ? where id = ?", nativeQuery = true)
    public void updatefanscount(int x, String friendid);
    @Modifying
    @Query(value = "update tb_user set followcount = followcount + ? where id = ?", nativeQuery = true)
    public void updatefollwcount(int x, String userid);

    @Modifying
    @Query(value = "update tb_user set status =  ? where id = ?", nativeQuery = true)
    void updateStatus(String status,String id);

    @Modifying
    @Query(value = "update tb_user set nickname =  ?,avatar =  ?,sex =  ?,personality =  ?,birthday =  ?,interest =  ?,address =  ?,updatedate =  ? where id = ?", nativeQuery = true)
    void updateUserinfo(String nickname, String avatar, String sex, String personality, Date birthday,String interest,String address,Date updatedate,String id);
}
