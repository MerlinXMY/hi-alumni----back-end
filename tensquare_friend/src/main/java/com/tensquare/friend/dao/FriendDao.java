package com.tensquare.friend.dao;

import com.tensquare.friend.pojo.Friend;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface FriendDao extends JpaRepository<Friend,String> {

    public Friend findByUseridAndFriendid(String userid,String friendid);
    @Modifying
    @Query(value = "UPDATE  tb_friend SET islike=? WHERE userid=? AND friendid=?",nativeQuery = true)
    public void updateIslike(String islike,String userid,String friendid);

    @Modifying
    @Query(value = "DELETE FROM tb_friend WHERE userid=? AND friendid=?",nativeQuery = true)
    public void deletefriend(String userid, String friendid);

    @Query(value = "SELECT * FROM tb_friend WHERE userid=? AND islike = '1'",nativeQuery = true)
    public Page<Friend> findByUserid(String userid, Pageable pageable);

    @Query(value = " SELECT " +
            "friend.id AS id," +
            "friend.userid," +
            "friend.friendid," +
            "friend.islike ," +
            "friend.createTime," +
            "friend.updateTime," +
            "user.nickname," +
            "user.sex," +
            "user.birthday," +
            "user.avatar " +
            "FROM tensquare_friend.tb_friend friend " +
            "LEFT JOIN tensquare_user.tb_user `user` ON  friend.userid=user.id " +
            " WHERE friend.friendid = ? ORDER BY friend.createTime DESC",nativeQuery = true)
    Page<Map<String,Object>> findRequestFriendsInfo(String userid, Pageable pageable);
    @Query(value = "SELECT count(*) FROM tb_friend WHERE userid=? AND friendid=? AND islike = '1'",nativeQuery = true)
    int isFriend(String userid, String friendid);
}
