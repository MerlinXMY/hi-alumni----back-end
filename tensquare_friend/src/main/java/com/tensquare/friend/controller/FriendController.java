package com.tensquare.friend.controller;

import com.tensquare.friend.client.UserClient;
import com.tensquare.friend.pojo.Friend;
import com.tensquare.friend.service.FriendService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/friend")
public class FriendController {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private FriendService friendService;

    @Autowired
    private UserClient userClient;

    /**
     * 添加好友或非好友
     * @return
     */
    @RequestMapping(value = "/like/{friendId}/{type}",method = RequestMethod.PUT)
    public Result addFriend(@PathVariable String friendId,@PathVariable String type){

        //验证是否登录，并且拿到当前登录用户的Id
        Claims claims = (Claims) request.getAttribute("claims_user");
        if(claims == null){
            return new Result(false,StatusCode.LOGINERROR,"权限不足");
        }
        //得到当前登录的用户id
        String userid = claims.getId();
        //判断是添加好友还是非好友
        if(type!=null){
            if(type.equals("1")){
                //添加好友

               int flag =  friendService.addFriend(userid,friendId);
               if(flag == 0){
                   return new Result(false, StatusCode.ERROR,"不能重复添加好友");
               }
               if(flag == 1){
                   userClient.updateFansCountAndFollowCount(userid,friendId,1);
                   return new Result(true, StatusCode.OK,"添加成功");
               }
                if(flag == 2){
                    return new Result(false, StatusCode.ERROR,"不能添加自己为好友 ");
                }
                if(flag == 3){
                    return new Result(false, StatusCode.ERROR,"已添加该好友 ");
                }
            }else if(type.equals("2")){
                //添加非好友
               int flag = friendService.addNoFriend(userid,friendId);
               if(flag == 0){
                   return new Result(false, StatusCode.ERROR,"不能重复拉黑");
               }
               if(flag == 1){
                    return new Result(true, StatusCode.OK,"拉黑成功");
                }
                if(flag == 2){
                    return new Result(false, StatusCode.ERROR,"不能拉黑自己");
                }


            }
            return new Result(false, StatusCode.ERROR,"参数异常");
        }else{
            return new Result(false, StatusCode.ERROR,"参数异常");
        }

    }

    @RequestMapping(value = "/{friendid}",method = RequestMethod.DELETE)
    public Result deleteFriend(@PathVariable String friendid){
        //验证是否登录，并且拿到当前登录用户的Id
        Claims claims = (Claims) request.getAttribute("claims_user");
        if(claims == null){
            return new Result(false,StatusCode.LOGINERROR,"权限不足");
        }
        //得到当前登录的用户id
        String userid = claims.getId();

        friendService.deleteFriend(userid,friendid);
        userClient.updateFansCountAndFollowCount(userid,friendid,-1);

        return new Result(true, StatusCode.OK,"删除成功");

    }

    @RequestMapping(value = "/findFriends/{page}/{size}",method = RequestMethod.GET)
    public Result findFriends(@PathVariable int page,@PathVariable int size){
        //验证是否登录，并且拿到当前登录用户的Id
        Claims claims = (Claims) request.getAttribute("claims_user");
        if(claims == null){
            return new Result(false,StatusCode.LOGINERROR,"权限不足");
        }
        //得到当前登录的用户id
        String userid = claims.getId();
        Page<Friend> pageData= friendService.findByUserid(userid,page,size);
        return new Result(true,StatusCode.OK,"查询成功",new PageResult<Friend>(pageData.getTotalElements(),pageData.getContent()));

    }

    @RequestMapping(value = "/findRequestFriendsInfo/{page}/{size}")
    public Result findRequestFriendsInfo(@PathVariable int page,@PathVariable int size){
        //验证是否登录，并且拿到当前登录用户的Id
        Claims claims = (Claims) request.getAttribute("claims_user");
        if(claims == null){
            return new Result(false,StatusCode.LOGINERROR,"权限不足");
        }
        //得到当前登录的用户id
        String userid = claims.getId();
        Page<Map<String,Object>> pageData= friendService.findRequestFriendsInfo(userid,page,size);
        return new Result(true,StatusCode.OK,"查询成功",new PageResult<Map<String,Object>>(pageData.getTotalElements(),pageData.getContent()));
    }

    @RequestMapping(value = "/isFriend/{friendid}")
    public Result isFriend( @PathVariable String friendid){
        //验证是否登录，并且拿到当前登录用户的Id
        Claims claims = (Claims) request.getAttribute("claims_user");
        if(claims == null){
            return new Result(false,StatusCode.LOGINERROR,"权限不足");
        }
        //得到当前登录的用户id
        String userid = claims.getId();

        return new Result(true,StatusCode.OK,"查询成功",friendService.isFriend(userid,friendid));
    }
}
