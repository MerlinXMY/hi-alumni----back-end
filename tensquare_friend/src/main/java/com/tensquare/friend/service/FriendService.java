package com.tensquare.friend.service;

import com.tensquare.friend.dao.FriendDao;
import com.tensquare.friend.dao.NoFriendDao;
import com.tensquare.friend.pojo.Friend;
import com.tensquare.friend.pojo.NoFriend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import util.IdWorker;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class FriendService {

    @Autowired
    private FriendDao friendDao;

    @Autowired
    private NoFriendDao noFriendDao;

    @Autowired
    private IdWorker idWorker;


    public int addFriend(String userid, String friendId) {
        //先判断userid到friendid是否有数据，有就是重复添加好友，返回0
        Friend friend = friendDao.findByUseridAndFriendid(userid, friendId);
        if(friend != null){
            if(friend.getIslike().equals("1")){
                return 3;
            }
            return 0;
        }
        if(userid == friendId){
            return 2;
        }

        //直接添加好友，让好友表中userid到friendid方向的type为0
        friend = new Friend();
        friend.setId(idWorker.nextId()+"");
        friend.setUserid(userid);
        friend.setFriendid(friendId);
        friend.setIslike("0");
        friend.setCreateTime(new Date());
        friendDao.save(friend);
        //判断从friendid到userid是否有数据，如果有，把双方的状态都改为1
        if(friendDao.findByUseridAndFriendid(friendId,userid)!=null){
            friendDao.updateIslike("1",userid,friendId);
            friendDao.updateIslike("1",friendId,userid);

        }
        return 1;


    }

    public int addNoFriend(String userid, String friendId) {

        //先判断是否已经是非好友
        NoFriend noFriend = noFriendDao.findByUseridAndFriendid(userid, friendId);
        if(noFriend != null){
            return 0;
        }
        if(userid == friendId){
            return 2;
        }
        noFriend = new NoFriend();
        noFriend.setUserid(userid);
        noFriend.setFriendid(friendId);
        noFriendDao.save(noFriend);
        return 1;


    }

    public void deleteFriend(String userid, String friendid) {

        //先删除好友表中userid到friendid这条数据
        friendDao.deletefriend(userid,friendid);

        //更新friendid到userid的islike为0
        friendDao.updateIslike("0",friendid,userid);
        NoFriend noFriend = new NoFriend();
        noFriend.setUserid(userid);
        noFriend.setFriendid(friendid);
        //非好友表中中添加数据
        noFriendDao.save(noFriend);

    }

    public Page<Friend> findByUserid(String userid,int page, int size) {

        Pageable pageable = PageRequest.of(page-1,size);
      return friendDao.findByUserid(userid,pageable);
    }

    public Page<Map<String,Object>> findRequestFriendsInfo(String userid, int page, int size) {
        Pageable pageable = PageRequest.of(page-1,size);
        return friendDao.findRequestFriendsInfo(userid,pageable);
    }

    public Boolean isFriend(String userid, String friendid) {
        int i = friendDao.isFriend(userid,friendid);
        if(i>0){
            return true;
        }else{
            return false;
        }

    }
}
