package com.tensquare.test.customer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "itcast")
public class Cutomer1 {

    @RabbitHandler
    public void getMsg(String msg){

        System.out.println("直接消费者消息"+msg);
    }
}
