package com.tensquare.square.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tensquare.square.pojo.Tags;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface TagsDao extends JpaRepository<Tags,String>,JpaSpecificationExecutor<Tags>{

    @Query(value = "SELECT * FROM tb_tags  ORDER BY `count` DESC ",nativeQuery = true)
    Page<Tags> findByHot(Pageable pageable);

    @Query(value = "SELECT * FROM tb_tags WHERE tag LIKE %?1% ORDER BY `count` DESC ",nativeQuery = true)
    List<Tags> findByTagLike(String tag);
}
