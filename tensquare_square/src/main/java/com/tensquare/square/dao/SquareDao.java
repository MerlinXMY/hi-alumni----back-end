package com.tensquare.square.dao;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tensquare.square.pojo.Square;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Map;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface SquareDao extends JpaRepository<Square,String>,JpaSpecificationExecutor<Square>{

    @Modifying
    @Query(value = "UPDATE tb_square SET state = ? WHERE id = ?", nativeQuery = true)
    void updateState(String state,String id);
    @Query(value = "SELECT " +
            "square.id AS id," +
            "square.square_class_id," +
            "square.user_id," +
            "square.content_text ," +
            "square.content_image," +
            "square.content_video," +
            "square.publictime," +
            "square.permission," +
            "square.visits_num," +
            "square.thumbup_num," +
            "square.comment_num," +
            "square.share_num," +
            "square.state," +
            "square.location_string," +
            "square.user_age," +
            "square.videoimageurl," +
            "user.nickname," +
            "user.sex," +
            "user.birthday," +
            "user.avatar " +
            "FROM tensquare_square.tb_square square " +
            "LEFT JOIN tensquare_user.tb_user `user` ON  square.user_id=user.id ORDER BY square.publictime DESC", nativeQuery = true)
    Page<Map<String,Object>> findSearchEarliest(Pageable pageable);

    @Modifying
    @Query(value = "UPDATE tb_square SET thumbup_num = thumbup_num + 1 WHERE id = ?", nativeQuery = true)
    int updateThumbupNum(String id);
    @Modifying
    @Query(value = "UPDATE tb_square SET thumbup_num = thumbup_num - 1 WHERE id = ?", nativeQuery = true)
    int updateDislike(String spitId);
    @Modifying
    @Query(value = "UPDATE tb_square SET visits_num = visits_num + 1 WHERE id = ?", nativeQuery = true)
    int updateVisit(String spitId);
}
