package com.tensquare.square.dao;

import com.tensquare.square.pojo.ActmappTags;
import com.tensquare.square.pojo.Square;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ActmappTagsDao extends JpaRepository<ActmappTags,String>, JpaSpecificationExecutor<ActmappTags> {
}
