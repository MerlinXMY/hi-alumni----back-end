package com.tensquare.square.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tensquare.square.pojo.SquareClass;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface SquareClassDao extends JpaRepository<SquareClass,String>,JpaSpecificationExecutor<SquareClass>{
	
}
