package com.tensquare.square.controller;

import com.tensquare.square.pojo.ActmappTags;
import com.tensquare.square.pojo.Square;
import com.tensquare.square.service.ActmappTagsService;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/actmapptags")
public class ActmappTagsController {

    @Autowired
    private ActmappTagsService actmappTagsService;

    @RequestMapping(method= RequestMethod.POST)
    public Result add(@RequestBody ActmappTags actmappTags) {
        actmappTagsService.add(actmappTags);
        return new Result(true, StatusCode.OK,"添加成功");
    }
}
