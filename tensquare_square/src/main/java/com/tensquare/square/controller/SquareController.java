package com.tensquare.square.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.tensquare.square.pojo.Tags;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tensquare.square.pojo.Square;
import com.tensquare.square.service.SquareService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.web.multipart.MultipartFile;
import util.FastDFSClient;
import util.FileUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/square")
public class SquareController {
	@Autowired
	private HttpServletRequest request;

	@Autowired
	private SquareService squareService;

	@Autowired
	FastDFSClient fastDFSClient;

	@Autowired
	private RedisTemplate redisTemplate;
	/**
	 * 查询全部数据
	 * @return
	 */
	@RequestMapping(method= RequestMethod.GET)
	public Result findAll(){
		return new Result(true,StatusCode.OK,"查询成功",squareService.findAll());
	}
	
	/**
	 * 根据ID查询
	 * @param id ID
	 * @return
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.GET)
	public Result findById(@PathVariable String id){
		return new Result(true,StatusCode.OK,"查询成功",squareService.findById(id));
	}


	/**
	 * 分页+多条件查询
	 * @param searchMap 查询条件封装
	 * @param page 页码
	 * @param size 页大小
	 * @return 分页结果
	 */
	@RequestMapping(value="/searchMap/{page}/{size}",method=RequestMethod.POST)
	public Result findSearchMap(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
		if(searchMap.get("square_class_id").equals("1")){
			Page<Map<String,Object>> pageList = squareService.findSearchEarliest(page, size);
			return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Map<String,Object>>(pageList.getTotalElements(), pageList.getContent()) );
		}else {
			return new Result(false,StatusCode.ERROR,"查询失败");
		}


	}

	/**
	 * 分页
	 *
	 * @param page 页码
	 * @param size 页大小
	 * @return 分页结果
	 */
	@RequestMapping(value="/search/{page}/{size}",method=RequestMethod.POST)
	public Result findSearch(@PathVariable int page, @PathVariable int size){
		Page<Square> pageList = squareService.findByPage(page,size);
		System.out.println(pageList.getContent());
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Square>(pageList.getTotalElements(), pageList.getContent()) );
	}

	/**
     * 根据条件查询
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/search",method = RequestMethod.POST)
    public Result findSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",squareService.findSearch(searchMap));
    }
	
	/**
	 * 增加
	 * @param square
	 */
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody Square square)  {
		System.out.println(square.getVideoimageurl());
		String square_id = squareService.add(square);
		Map<String,String> map = new HashMap<>();
		map.put("square_id",square_id);
		System.out.println(square.toString());
		return new Result(true, StatusCode.OK, "增加成功", map);
	}
	
	/**
	 * 修改
	 * @param square
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody Square square, @PathVariable String id ){
		square.setId(id);
		squareService.update(square);		
		return new Result(true,StatusCode.OK,"修改成功");
	}
	@RequestMapping(value="/undateState/{id}/{state}",method= RequestMethod.PUT)
	public Result update(@PathVariable String id,@PathVariable String state){
		squareService.updateState(state,id);
		return new Result(true,StatusCode.OK,"修改成功");
	}
	
	/**
	 * 删除
	 * @param id
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id ){
		squareService.deleteById(id);
		return new Result(true,StatusCode.OK,"删除成功");
	}

	@RequestMapping(value = "/thumbup/{spitId}",method = RequestMethod.PUT)
	public Result thumbup(@PathVariable String spitId){

		//判断当前用户是否点赞
		//验证是否登录，并且拿到当前登录用户的Id
		Claims claims = (Claims) request.getAttribute("claims_user");
		if(claims == null){
			return new Result(false,StatusCode.LOGINERROR,"权限不足");
		}
		//得到当前登录的用户id
		String userid = claims.getId();
		//判断当前用户是否已经点赞
		if(redisTemplate.opsForValue().get("thumbup_"+userid+"_"+spitId)!=null){
			return new Result(false, StatusCode.REPERROR,"不能重复点赞");
		}

		int flag = squareService.thumbup(spitId);
		if(flag > 0){
			redisTemplate.opsForValue().set("thumbup_"+userid+"_"+spitId,1);
			return new Result(true, StatusCode.OK,"点赞成功");
		}else{
			return new Result(false, StatusCode.ERROR,"点赞失败");
		}

	}
	@RequestMapping(value = "/dislike/{spitId}",method = RequestMethod.PUT)
	public Result dislike(@PathVariable String spitId){

		//判断当前用户是否点赞
		//验证是否登录，并且拿到当前登录用户的Id
		Claims claims = (Claims) request.getAttribute("claims_user");
		if(claims == null){
			return new Result(false,StatusCode.LOGINERROR,"权限不足");
		}
		//得到当前登录的用户id
		String userid = claims.getId();
		//判断当前用户是否已经点赞
		if(redisTemplate.opsForValue().get("thumbup_"+userid+"_"+spitId)!=null){
			int flag = squareService.dislike(spitId);
			if(flag>0){
				redisTemplate.opsForValue().set("thumbup_"+userid+"_"+spitId,null);
				return new Result(true, StatusCode.OK,"取消点赞");
			}

		}

		return new Result(false, StatusCode.REPERROR,"取消点赞失败");

	}
	@RequestMapping(value = "/isThumbup/{spitId}",method = RequestMethod.GET)
	public Result isThumbup(@PathVariable String spitId) {
		//判断当前用户是否点赞
		//验证是否登录，并且拿到当前登录用户的Id
		Claims claims = (Claims) request.getAttribute("claims_user");
		if(claims == null){
			return new Result(false,StatusCode.LOGINERROR,"权限不足");
		}
		//得到当前登录的用户id
		String userid = claims.getId();
		//判断当前用户是否已经点赞
		if(redisTemplate.opsForValue().get("thumbup_"+userid+"_"+spitId)!=null){
			return new Result(true, StatusCode.OK,"已点赞");
		}else{
			return new Result(false, StatusCode.ERROR,"未点赞");
		}
	}
	@RequestMapping(value = "/visit/{spitId}",method = RequestMethod.PUT)
	public Result visit(@PathVariable String spitId) {

		//判断当前用户是否浏览
		//验证是否登录，并且拿到当前登录用户的Id
		Claims claims = (Claims) request.getAttribute("claims_user");
		if(claims == null){
			return new Result(false,StatusCode.LOGINERROR,"权限不足");
		}
		//得到当前登录的用户id
		String userid = claims.getId();
		//判断当前用户是否已经访问
		if(redisTemplate.opsForValue().get("visit_"+userid+"_"+spitId)!=null){
			return new Result(false, StatusCode.REPERROR,"已访问过");
		}

		int flag = squareService.updateVisit(spitId);
		if(flag > 0){
			redisTemplate.opsForValue().set("visit_"+userid+"_"+spitId,1);
			return new Result(true, StatusCode.OK,"访问量更新成功");
		}else{
			return new Result(false, StatusCode.ERROR,"访问量更新失败");
		}
	}

}
