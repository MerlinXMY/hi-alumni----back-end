package com.tensquare.square.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tensquare.square.pojo.Tags;
import com.tensquare.square.service.TagsService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/tags")
public class TagsController {

	@Autowired
	private TagsService tagsService;
	
	
	/**
	 * 查询全部数据
	 * @return
	 */
	@RequestMapping(method= RequestMethod.GET)
	public Result findAll(){
		return new Result(true,StatusCode.OK,"查询成功",tagsService.findAll());
	}
	
	/**
	 * 根据ID查询
	 * @param id ID
	 * @return
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.GET)
	public Result findById(@PathVariable String id){
		return new Result(true,StatusCode.OK,"查询成功",tagsService.findById(id));
	}


	/**
	 * 分页+多条件查询
	 * @param searchMap 查询条件封装
	 * @param page 页码
	 * @param size 页大小
	 * @return 分页结果
	 */
	@RequestMapping(value="/search/{page}/{size}",method=RequestMethod.POST)
	public Result findSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
		Page<Tags> pageList = tagsService.findSearch(searchMap, page, size);
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Tags>(pageList.getTotalElements(), pageList.getContent()) );
	}

	@RequestMapping(value="/searchByHot/{page}/{size}",method=RequestMethod.GET)
	public Result findSearchByHot( @PathVariable int page, @PathVariable int size){
		Page<Tags> pageList = tagsService.findSearchByHot(page, size);
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Tags>(pageList.getTotalElements(), pageList.getContent()) );
	}

	@RequestMapping(value="/findsearchByTag",method=RequestMethod.POST)
	public Result findSearchByTag(@RequestBody Tags tags){
		List<Tags> list = tagsService.findSearch(tags.getTag());
		return  new Result(true,StatusCode.OK,"查询成功", list );
	}

	/**
     * 根据条件查询
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/search",method = RequestMethod.POST)
    public Result findSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",tagsService.findSearch(searchMap));
    }
	
	/**
	 * 增加
	 * @param tags
	 */
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody Tags tags  ){
		String tag_id = tagsService.add(tags);
		Map<String,String> map = new HashMap<>();
		map.put("id",tag_id);
		return new Result(true,StatusCode.OK,"增加成功",map);
	}
	
	/**
	 * 修改
	 * @param tags
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody Tags tags, @PathVariable String id ){
		tags.setId(id);
		tagsService.update(tags);		
		return new Result(true,StatusCode.OK,"修改成功");
	}
	
	/**
	 * 删除
	 * @param id
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id ){
		tagsService.deleteById(id);
		return new Result(true,StatusCode.OK,"删除成功");
	}
	
}
