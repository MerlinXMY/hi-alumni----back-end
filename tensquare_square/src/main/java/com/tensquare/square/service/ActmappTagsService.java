package com.tensquare.square.service;

import com.tensquare.square.dao.ActmappTagsDao;
import com.tensquare.square.pojo.ActmappTags;
import com.tensquare.square.pojo.Tags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.IdWorker;

@Service
public class ActmappTagsService {

    @Autowired
    private ActmappTagsDao actmappTagsDao;

    @Autowired
    private IdWorker idWorker;

    public void add(ActmappTags actmappTags) {
        String id = idWorker.nextId()+"";
        actmappTags.setId(id);
        actmappTagsDao.save(actmappTags);
    }
}
