package com.tensquare.square.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.tensquare.square.dao.SquareClassDao;
import com.tensquare.square.pojo.SquareClass;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class SquareClassService {

	@Autowired
	private SquareClassDao squareClassDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<SquareClass> findAll() {
		return squareClassDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<SquareClass> findSearch(Map whereMap, int page, int size) {
		Specification<SquareClass> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return squareClassDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<SquareClass> findSearch(Map whereMap) {
		Specification<SquareClass> specification = createSpecification(whereMap);
		return squareClassDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public SquareClass findById(String id) {
		return squareClassDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param squareClass
	 */
	public void add(SquareClass squareClass) {
		squareClass.setId( idWorker.nextId()+"" );
		squareClassDao.save(squareClass);
	}

	/**
	 * 修改
	 * @param squareClass
	 */
	public void update(SquareClass squareClass) {
		squareClassDao.save(squareClass);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		squareClassDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<SquareClass> createSpecification(Map searchMap) {

		return new Specification<SquareClass>() {

			@Override
			public Predicate toPredicate(Root<SquareClass> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // ID
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 专栏名称
                if (searchMap.get("name")!=null && !"".equals(searchMap.get("name"))) {
                	predicateList.add(cb.like(root.get("name").as(String.class), "%"+(String)searchMap.get("name")+"%"));
                }
                // 状态
                if (searchMap.get("state")!=null && !"".equals(searchMap.get("state"))) {
                	predicateList.add(cb.like(root.get("state").as(String.class), "%"+(String)searchMap.get("state")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
