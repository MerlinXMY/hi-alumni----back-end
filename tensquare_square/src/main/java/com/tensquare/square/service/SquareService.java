package com.tensquare.square.service;

import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import util.IdWorker;

import com.tensquare.square.dao.SquareDao;
import com.tensquare.square.pojo.Square;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
@Transactional
public class SquareService {

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private SquareDao squareDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Square> findAll() {
		return squareDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Square> findSearch(Map whereMap, int page, int size) {
		Sort sort = new Sort(Sort.Direction.DESC,"publictime");
		Specification<Square> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size,sort);
		return squareDao.findAll(specification, pageRequest);
	}

	public Page<Map<String,Object>> findSearchEarliest(int page, int size) {
		Pageable pageable = PageRequest.of(page-1,size);

		return squareDao.findSearchEarliest(pageable);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Square> findSearch(Map whereMap) {
		Specification<Square> specification = createSpecification(whereMap);
		return squareDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Square findById(String id) {
		return squareDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param square
	 */
	public String add(Square square) {
		String id = idWorker.nextId()+"";
		square.setId(id);
		square.setThumbup_num(0);
		square.setVisits_num(0);
		square.setComment_num(0);
		squareDao.save(square);
		return id;
	}

	/**
	 * 修改
	 * @param square
	 */
	public void update(Square square) {
		squareDao.save(square);
	}
	public void updateState(String state,String id) {
		squareDao.updateState(state,id);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		squareDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Square> createSpecification(Map searchMap) {

		return new Specification<Square>() {

			@Override
			public Predicate toPredicate(Root<Square> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // ID
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 专栏ID
                if (searchMap.get("square_class_id")!=null && !"".equals(searchMap.get("square_class_id"))) {
                	predicateList.add(cb.like(root.get("square_class_id").as(String.class), "%"+(String)searchMap.get("square_class_id")+"%"));
                }
                // 用户ID
                if (searchMap.get("user_id")!=null && !"".equals(searchMap.get("user_id"))) {
                	predicateList.add(cb.like(root.get("user_id").as(String.class), "%"+(String)searchMap.get("user_id")+"%"));
                }
                // 用户性别
                if (searchMap.get("user_sex")!=null && !"".equals(searchMap.get("user_sex"))) {
                	predicateList.add(cb.like(root.get("user_sex").as(String.class), "%"+(String)searchMap.get("user_sex")+"%"));
                }
                // 用户头像
                if (searchMap.get("user_pic")!=null && !"".equals(searchMap.get("user_pic"))) {
                	predicateList.add(cb.like(root.get("user_pic").as(String.class), "%"+(String)searchMap.get("user_pic")+"%"));
                }
                // 用户昵称
                if (searchMap.get("user_nickname")!=null && !"".equals(searchMap.get("user_nickname"))) {
                	predicateList.add(cb.like(root.get("user_nickname").as(String.class), "%"+(String)searchMap.get("user_nickname")+"%"));
                }
                // 校园广场正文
                if (searchMap.get("content_text")!=null && !"".equals(searchMap.get("content_text"))) {
                	predicateList.add(cb.like(root.get("content_text").as(String.class), "%"+(String)searchMap.get("content_text")+"%"));
                }
                // 校园广场图片
                if (searchMap.get("content_image")!=null && !"".equals(searchMap.get("content_image"))) {
                	predicateList.add(cb.like(root.get("content_image").as(String.class), "%"+(String)searchMap.get("content_image")+"%"));
                }
                // 校园广场视频
                if (searchMap.get("content_video")!=null && !"".equals(searchMap.get("content_video"))) {
                	predicateList.add(cb.like(root.get("content_video").as(String.class), "%"+(String)searchMap.get("content_video")+"%"));
                }
                // 权限
                if (searchMap.get("permission")!=null && !"".equals(searchMap.get("permission"))) {
                	predicateList.add(cb.like(root.get("permission").as(String.class), "%"+(String)searchMap.get("permission")+"%"));
                }
                // 审核状态
                if (searchMap.get("state")!=null && !"".equals(searchMap.get("state"))) {
                	predicateList.add(cb.like(root.get("state").as(String.class), "%"+(String)searchMap.get("state")+"%"));
                }
                // 位置
                if (searchMap.get("location_string")!=null && !"".equals(searchMap.get("location_string"))) {
                	predicateList.add(cb.like(root.get("location_string").as(String.class), "%"+(String)searchMap.get("location_string")+"%"));
                }
                // 经度
                if (searchMap.get("longitade")!=null && !"".equals(searchMap.get("longitade"))) {
                	predicateList.add(cb.like(root.get("longitade").as(String.class), "%"+(String)searchMap.get("longitade")+"%"));
                }
                // 纬度
                if (searchMap.get("latitade")!=null && !"".equals(searchMap.get("latitade"))) {
                	predicateList.add(cb.like(root.get("latitade").as(String.class), "%"+(String)searchMap.get("latitade")+"%"));
                }
                // 情感
                if (searchMap.get("emotion")!=null && !"".equals(searchMap.get("emotion"))) {
                	predicateList.add(cb.like(root.get("emotion").as(String.class), "%"+(String)searchMap.get("emotion")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

    public Page<Square> findByPage(int page, int size) {
		Sort sort = new Sort(Sort.Direction.DESC,"public_time");
		Pageable pageable = PageRequest.of(page - 1, size,sort);
		return squareDao.findAll(pageable);
    }

    public int  thumbup(String spitId) {

		return squareDao.updateThumbupNum(spitId);

    }

	public int dislike(String spitId) {
		return squareDao.updateDislike(spitId);
	}

	public int updateVisit(String spitId) {
		return squareDao.updateVisit(spitId);
	}
}
