package com.tensquare.square.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="tb_tags")
public class Tags implements Serializable{

	@Id
	private String id;//ID


	
	private String tag;//标签名称
	private String state;//状态
	private Long count;//使用数量

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getTag() {		
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getState() {		
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	public Long getCount() {		
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}


	
}
