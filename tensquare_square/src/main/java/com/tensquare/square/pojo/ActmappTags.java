package com.tensquare.square.pojo;

import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "tb_actmapptags")
public class ActmappTags  implements Serializable {
    @Id
    private String id;

    private String square_id;

    private String tags_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSquare_id() {
        return square_id;
    }

    public void setSquare_id(String square_id) {
        this.square_id = square_id;
    }

    public String getTags_id() {
        return tags_id;
    }

    public void setTags_id(String tags_id) {
        this.tags_id = tags_id;
    }
}
