package com.tensquare.square.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;


/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="tb_square")
public class Square implements Serializable{

	@Id
	private String id;//ID
	private String square_class_id;//专栏ID
	private String user_id;//用户ID
	private String content_text;//校园广场正文
	private String content_image;//校园广场图片
	private String content_video;//校园广场视频
	private Date publictime;//发布日期
	private String permission;//权限
	private Integer visits_num;//浏览量
	private Integer thumbup_num;//点赞数
	private Integer comment_num;//评论数
	private Integer share_num;//分享数
	private String state;//审核状态
	private String location_string;//位置
	private String longitade;//经度
	private String latitade;//纬度
	private String emotion;//情感
	private Integer user_age;//年龄
	private String videoimageurl;

	public String getVideoimageurl() {
		return videoimageurl;
	}

	public void setVideoimageurl(String videoimageurl) {
		this.videoimageurl = videoimageurl;
	}

	public Date getPublictime() {
		return publictime;
	}

	public void setPublictime(Date publictime) {
		this.publictime = publictime;
	}

	public Integer getUser_age() {
		return user_age;
	}

	public void setUser_age(Integer user_age) {
		this.user_age = user_age;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getSquare_class_id() {		
		return square_class_id;
	}
	public void setSquare_class_id(String square_class_id) {
		this.square_class_id = square_class_id;
	}

	public String getUser_id() {		
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getContent_text() {		
		return content_text;
	}
	public void setContent_text(String content_text) {
		this.content_text = content_text;
	}

	public String getContent_image() {		
		return content_image;
	}
	public void setContent_image(String content_image) {
		this.content_image = content_image;
	}

	public String getContent_video() {		
		return content_video;
	}
	public void setContent_video(String content_video) {
		this.content_video = content_video;
	}


	public String getPermission() {		
		return permission;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}

	public Integer getVisits_num() {		
		return visits_num;
	}
	public void setVisits_num(Integer visits_num) {
		this.visits_num = visits_num;
	}

	public Integer getThumbup_num() {		
		return thumbup_num;
	}
	public void setThumbup_num(Integer thumbup_num) {
		this.thumbup_num = thumbup_num;
	}

	public Integer getComment_num() {		
		return comment_num;
	}
	public void setComment_num(Integer comment_num) {
		this.comment_num = comment_num;
	}

	public Integer getShare_num() {		
		return share_num;
	}
	public void setShare_num(Integer share_num) {
		this.share_num = share_num;
	}

	public String getState() {		
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	public String getLocation_string() {		
		return location_string;
	}
	public void setLocation_string(String location_string) {
		this.location_string = location_string;
	}

	public String getLongitade() {		
		return longitade;
	}
	public void setLongitade(String longitade) {
		this.longitade = longitade;
	}

	public String getLatitade() {		
		return latitade;
	}
	public void setLatitade(String latitade) {
		this.latitade = latitade;
	}

	public String getEmotion() {		
		return emotion;
	}
	public void setEmotion(String emotion) {
		this.emotion = emotion;
	}


	
}
