package com.tensquare.chat.controller;

import com.tensquare.chat.enityVo.Message;
import com.tensquare.chat.enityVo.UserVo;
import com.tensquare.chat.service.HuanXinService;
import com.tensquare.chat.service.HuanXinTokenService;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(value = "/chat")
public class ChatControlller {

    @Autowired
    private HuanXinTokenService huanXinTokenService;

    @Autowired
    private HuanXinService huanXinService;

    @Autowired
    private BCryptPasswordEncoder encoder;


    @RequestMapping(value = "/getHuanXinToken")
    public Result getHuanXinToken(){
        String token = huanXinTokenService.getToken();
        return new Result(true, StatusCode.OK,"获取token成功",token);
    }
    @RequestMapping(value = "/register" , method = RequestMethod.POST)
    public String register(@RequestBody Map map){
        System.out.println("进入IM注册");
        System.out.println(map.get("username"));
        if(huanXinService.register((String) map.get("username"))){
            return  "scuess";
        }else{
            return  "fail";
        }
    }

    @RequestMapping(value = "/user/{id}")
    public Result queryUser(@PathVariable String id){

        UserVo userVo = new UserVo();
        userVo.setUsername(id);
        userVo.setPassword(encoder.encode(id+"hiSchoolMate"));
        return new Result(true,StatusCode.OK,"查询成功",userVo);
    }

    @RequestMapping(value = "/contacts/{userId}/{friendId}")
    public String contactUsers(@PathVariable String userId ,@PathVariable String friendId){

        boolean result = huanXinService.contactUsers(userId, friendId);
        if(result){
            return  "scuess";
        }else{
            return  "fail";
        }
    }

}
