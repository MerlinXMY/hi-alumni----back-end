package com.tensquare.chat.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tensquare.chat.config.HuanXinConfig;
import com.tensquare.chat.enityVo.Message;
import com.tensquare.chat.enityVo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class HuanXinService {
    @Autowired
    private HuanXinConfig huanXinConfig;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HuanXinTokenService huanXinTokenService;

    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * 注册环信用户
     */

    public boolean register(String id){
        String url = this.huanXinConfig.getUrl()
                +this.huanXinConfig.getOrgName()+"/"
                +this.huanXinConfig.getAppName()+"/users";
        String token = this.huanXinTokenService.getToken();
        System.out.println(url);
        //请求头信息
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type","application/json");
        httpHeaders.add("Authorization","Bearer "+token);
        //请求体
        List<UserVo>  userVos = new ArrayList<>();
        userVos.add(new UserVo(id,id));
        try {
            HttpEntity<String> httpEntity = new HttpEntity(MAPPER.writeValueAsString(userVos),httpHeaders);
           //发起请求
            ResponseEntity<String> responseEntity = this.restTemplate.postForEntity(url, httpEntity, String.class);
            return responseEntity.getStatusCodeValue() == 200;

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return false;


    }

    public boolean contactUsers(String userId,String friendId){
        String url = this.huanXinConfig.getUrl()
                +this.huanXinConfig.getOrgName()+"/"
                +this.huanXinConfig.getAppName()+"/users/"
                +userId + "/contacts/users/" + friendId;
        String token = this.huanXinTokenService.getToken();
        System.out.println(url);
        //请求头信息
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type","application/json");
        httpHeaders.add("Authorization","Bearer "+token);
        //请求体
        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
        try{
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, httpEntity, String.class);
            return responseEntity.getStatusCodeValue() == 200;
        }catch (Exception e){
            e.printStackTrace();
        }

        return false;

    }


}
