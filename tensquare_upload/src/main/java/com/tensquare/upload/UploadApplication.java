package com.tensquare.upload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import util.FastDFSClient;
import util.IdWorker;
import util.VideoImageUtil;


@SpringBootApplication
public class UploadApplication {
    public static void main(String[] args) {
        SpringApplication.run(UploadApplication.class,args);
    }

    @Bean
    public IdWorker idWorkker(){
        return new IdWorker(1, 1);
    }

    @Bean
    public FastDFSClient fastDFSClient(){
        return new FastDFSClient();
    }

    @Bean
    public VideoImageUtil videoImageUtil(){
        return new VideoImageUtil();
    }

}
