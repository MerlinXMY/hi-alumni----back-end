package com.tensquare.upload.controller;


import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import util.FastDFSClient;
import util.VideoImageUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/upload")
public class UploadController {
    @Autowired
    FastDFSClient fastDFSClient;
    @Autowired
    VideoImageUtil videoImageUtil;

    @RequestMapping(method = RequestMethod.POST)
    public Result FileUpload(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        if(multipartFile.isEmpty()){
            return new Result(false, StatusCode.ERROR,"上传文件为空！");
        }

        String url =  fastDFSClient.uploadSquareImg(multipartFile);
        url = "http://192.168.43.238:88/wdzy/" + url;
        return new Result(true,StatusCode.OK,"上传成功",url);
    }

    @RequestMapping(value = "/videoLoad",method = RequestMethod.POST)
    public Result FileUploadVideo(@RequestParam("file") MultipartFile multipartFile) throws Exception {
        if(multipartFile.isEmpty()){
            return new Result(false, StatusCode.ERROR,"上传文件为空！");
        }
        Map<String,String> map = new  HashMap<>();
        String url =  fastDFSClient.uploadSquareVideo(multipartFile);
        url = "http://192.168.43.238:88/wdzy/" + url;
        MultipartFile multipartFile1 = VideoImageUtil.fetchFrame(url);
        if(multipartFile1.isEmpty()){
            return new Result(false, StatusCode.ERROR,"上传文件为空！");
        }else
        {
            String VideoImageurl =  fastDFSClient.uploadSquareImg(multipartFile1);
            VideoImageurl = "http://192.168.43.238:88/wdzy/" + VideoImageurl;
            map.put("videoImageUrl",VideoImageurl);
        }

        map.put("videoUrl",url);

        return new Result(true,StatusCode.OK,"上传成功",map);
    }
}
